Shader "Custom/Debug" {
	Properties {
		_Color( "Color", Color ) = ( 1.0, 1.0, 1.0, 1.0 )
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		LOD 200

		Pass {
			CGPROGRAM
				fixed4 _Color;

				#pragma vertex vert
				#pragma fragment frag

				float4 vert( float4 v : POSITION ) : SV_POSITION {
					return mul( UNITY_MATRIX_MVP, v );
				}

				fixed4 frag() : COLOR {
					return _Color;
				}
			ENDCG
		}
	}
	FallBack "Diffuse"
}