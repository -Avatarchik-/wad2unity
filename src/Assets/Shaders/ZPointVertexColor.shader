﻿Shader "Custom/ZPointVertexColor" {
	Properties {
		_PointSize ( "Point Size", Float ) = 1.0
	}
    SubShader {
    Pass {
        LOD 200
         
        CGPROGRAM

        #pragma vertex vert
        #pragma fragment frag
		#include "UnityCG.cginc"

		uniform float _PointSize;
 
        struct VertexInput {
            float4 vertex : POSITION;
            float4 color: COLOR;
        };
         
        struct VertexOutput {
            float4 pos : SV_POSITION;
            float4 col : COLOR;
			float psize : PSIZE;
        };
         
        VertexOutput vert( VertexInput v ) {
         
            VertexOutput o;
            o.pos = mul( UNITY_MATRIX_MVP, v.vertex );
            o.col = v.color;
			
			float4 scrpos = ComputeScreenPos( o.pos );
			COMPUTE_EYEDEPTH( scrpos.z );
			
			o.psize = _PointSize * (_ProjectionParams.y / scrpos.z * _ScreenParams.y );
             
            return o;
        }
         
        float4 frag(VertexOutput o) : COLOR {
            return o.col;
        }
 
        ENDCG
        } 
    }
 
}