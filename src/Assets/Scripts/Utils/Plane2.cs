﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public struct Plane2
{
	public Vector2 n;
	public float d;

	public Plane2( Vector2 normal, float distance )
	{
		n = normal;
		d = distance;
	}

	public static Plane2 CreateByNormalizedRay( Ray2D ray )
	{
		Vector2 normal = MathUtils.Perp( ray.direction );
		float dot = Vector2.Dot( ray.origin, normal );
		
		return new Plane2( normal, dot );
	}

	public float DistanceTo( Vector2 point )
	{
		float dot = Vector2.Dot( point, n );
		return Mathf.Abs( dot - d );
	}
}
