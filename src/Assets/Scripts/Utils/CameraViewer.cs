﻿using UnityEngine;
using System.Collections;

public class CameraViewer : MonoBehaviour
{
	public const float RUN_MULTIPLIER = 4f;
	public Transform watchTarget;

	public float walkSpeed = 25f;
	public float rotationSpeed = 120f;

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		float dt = Time.fixedDeltaTime;

		if ( watchTarget )
		{
		}
		else
		{
			if ( Input.GetKey( KeyCode.Mouse1 ) )
			{
				float rotMagnitude = rotationSpeed * dt;
				float rotX = Input.GetAxis( "Mouse Y" ) * rotMagnitude;
				float rotY = Input.GetAxis( "Mouse X" ) * rotMagnitude;

				Vector3 euler = transform.localRotation.eulerAngles;
				
				euler.x -= rotX;
				euler.y += rotY;
				euler.z = 0f;

				transform.localRotation = Quaternion.Euler( euler );
			}

			float moveMagnitude = walkSpeed * dt;
			if ( Input.GetKey( KeyCode.LeftShift ) || Input.GetKey( KeyCode.RightShift ) )
				moveMagnitude *= RUN_MULTIPLIER;

			float moveZ = Input.GetAxis( "Vertical" ) * moveMagnitude;
			float moveX = Input.GetAxis( "Horizontal" ) * moveMagnitude;

			transform.Translate( moveX, 0f, moveZ, Space.Self );
		}
	}
}