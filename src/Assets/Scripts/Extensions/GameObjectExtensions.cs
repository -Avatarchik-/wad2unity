using UnityEngine;
using System.Collections.Generic;
using System.IO;

public static class GameObjectExtensions
{
	public static void SetMeshToGameObject( this GameObject obj, Mesh mesh, Material mat )
	{
		MeshFilter mf = obj.GetComponent<MeshFilter>();
		if ( mf == null )
			mf = obj.AddComponent<MeshFilter>();
		mf.mesh = mesh;

		SetMaterialToGameObject( obj, mat );
	}

	public static void SetMaterialToGameObject( this GameObject obj, Material material )
	{
		SetMaterialsToGameObject( obj, new Material[ 1 ] { material } );
	}

	public static void SetMaterialsToGameObject( this GameObject obj, Material[] materials )
	{
		MeshRenderer mr = obj.GetComponent<MeshRenderer>();
		if ( mr == null )
			mr = obj.AddComponent<MeshRenderer>();
		if ( materials != null )
			mr.materials = materials;
	}

	public static void SetSharedMaterialToGameObject( this GameObject obj, Material material )
	{
		SetSharedMaterialsToGameObject( obj, new Material[ 1 ] { material } );
	}

	public static void SetSharedMaterialsToGameObject( this GameObject obj, Material[] materials )
	{
		MeshRenderer mr = obj.GetComponent<MeshRenderer>();
		if ( mr == null )
			mr = obj.AddComponent<MeshRenderer>();
		if ( materials != null )
			mr.sharedMaterials = materials;
	}

	public static GameObject CreateChild( this GameObject obj, string name )
	{
		GameObject go = new GameObject( name );
		go.transform.parent = obj.transform;
		go.transform.localPosition = Vector3.zero;
		go.transform.localRotation = Quaternion.identity;
		return go;
	}

}