﻿using UnityEngine;
using System.Collections.Generic;

namespace DoomWad
{
	[ExecuteInEditMode]
	public class MapFlat : MapObject
	{
		public List<MapSide> sides = new List<MapSide>();

#if UNITY_EDITOR
		// maps vertex index [local] -> [global]
		public int[] vertexIndices;
#endif

		public float height { get { return transform.localPosition.y; } }
		private float prevHeight;

		protected new void Awake()
		{
			base.Awake();
		}

		protected void Start()
		{
			prevHeight = height;
		}

		// Update is called once per frame
		protected new void Update()
		{
			base.Update();

			if ( height != prevHeight )
			{
				prevHeight = height;
				foreach ( MapSide s in sides )
					s.UpdateMesh();
			}
		}

		public Vector3 GetWorldVertex( int i )
		{
			return transform.localToWorldMatrix.MultiplyPoint(  mesh.vertices[ i ] );
		}

		//void OnDrawGizmos()
		//{
		//	if ( UnityEditor.Selection.Contains( gameObject ) )
		//	{
		//		Gizmos.matrix = Matrix4x4.identity;
		//		Gizmos.color = Color.green;
		//		foreach ( Vector3 v in mesh.vertices )
		//			Gizmos.DrawWireCube( transform.localToWorldMatrix.MultiplyPoint( v ), Vector3.one * 0.5f );
		//	}
		//}
	}

}