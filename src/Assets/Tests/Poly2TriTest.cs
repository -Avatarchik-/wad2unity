﻿using UnityEngine;
using System.Collections.Generic;
using Poly2Tri;

public class Poly2TriTest : MonoBehaviour
{
	public enum EditMode { POLY, HOLE, TRI }

	public float gridSize = 0.1f;

	public List<Vector2> poly = new List<Vector2>();
	public List<Vector2> hole = new List<Vector2>();

	public GUIText help;

	private Vector2 worldMousePosition;
	private EditMode mode = EditMode.POLY;

	private Polygon triPoly;
	private Polygon triHole;


	// Use this for initialization
	void Start()
	{
		worldMousePosition = UnityUtils.ScreenPointToWorldXY( Input.mousePosition );
		worldMousePosition = MathUtils.SnapPointToGrid( worldMousePosition, gridSize );

		UpdateHelpText();
	}

	// Update is called once per frame
	void Update()
	{
		worldMousePosition = UnityUtils.ScreenPointToWorldXY( Input.mousePosition );
		worldMousePosition = MathUtils.SnapPointToGrid( worldMousePosition, gridSize );

		if ( Input.GetKeyDown( KeyCode.P ) ) SetMode( EditMode.POLY );
		if ( Input.GetKeyDown( KeyCode.H ) ) SetMode( EditMode.HOLE );
		if ( Input.GetKeyDown( KeyCode.T ) ) SetMode( EditMode.TRI );
		
		
		if ( Input.GetKeyDown( KeyCode.Mouse0 ) )
		{
			if ( mode == EditMode.POLY )
				poly.Add( worldMousePosition );

			if ( mode == EditMode.HOLE )
				hole.Add( worldMousePosition );
		}
	}

	void OnDrawGizmos()
	{
		if ( !Application.isPlaying )
			return;

		if ( mode == EditMode.TRI )
		{
			Gizmos.matrix = Matrix4x4.identity;
			Gizmos.color = Color.white;
			
			foreach ( DelaunayTriangle t in triPoly.Triangles )
			{
				Vector2[] triangle = new Vector2[ 3 ];

				triangle[ 0 ] = new Vector2( ( float ) t.Points[ 0 ].X, ( float ) t.Points[ 0 ].Y );
				triangle[ 1 ] = new Vector2( ( float ) t.Points[ 1 ].X, ( float ) t.Points[ 1 ].Y );
				triangle[ 2 ] = new Vector2( ( float ) t.Points[ 2 ].X, ( float ) t.Points[ 2 ].Y );

				DebugDrawUtils.DrawWirePolygon( triangle, AxisPlane.XY, 0.1f );
			}
		}
		else
		{
			Gizmos.matrix = Matrix4x4.identity;
			Gizmos.color = Color.white;
			Gizmos.DrawWireCube( worldMousePosition, Vector3.one * 0.2f );

			Gizmos.color = Color.green;
			DebugDrawUtils.DrawWirePolygon( poly.ToArray(), AxisPlane.XY, 0.1f );

			Gizmos.color = Color.red;
			DebugDrawUtils.DrawWirePolygon( hole.ToArray(), AxisPlane.XY, 0.1f );
		}
	}

	private void SetMode( EditMode mode )
	{
		if ( mode == EditMode.TRI && this.mode != EditMode.TRI )
		{
			PolygonPoint[] triPolyPoints = new PolygonPoint[ poly.Count ];
			for ( int i = 0; i < poly.Count;i++ )
			{
				triPolyPoints[ i ] = new PolygonPoint( poly[ i ].x, poly[ i ].y );
			}

			PolygonPoint[] triHolePoints = new PolygonPoint[ hole.Count ];
			for ( int i = 0; i < hole.Count; i++ )
			{
				triHolePoints[ i ] = new PolygonPoint( hole[ i ].x, hole[ i ].y );
			}

			triPoly = new Polygon( triPolyPoints );
			triHole = new Polygon( triHolePoints );
			triPoly.AddHole( triHole );

			DTSweepContext tcx = new DTSweepContext();
			tcx.PrepareTriangulation( triPoly );
			DTSweep.Triangulate( tcx );
		}

		this.mode = mode;
		
		UpdateHelpText();
	}
	
	private void UpdateHelpText()
	{
		help.text =
			"Controls:\n" +
			"  Change Mode:\n";

		if ( mode == EditMode.POLY ) help.text += "<color=#F00>";
		help.text += "  <P> - Polygon Drawing\n";
		if ( mode == EditMode.POLY ) help.text += "</color>";

		if ( mode == EditMode.HOLE ) help.text += "<color=#F00>";
		help.text += "  <H> - Hole Drawing\n";
		if ( mode == EditMode.HOLE ) help.text += "</color>";

		if ( mode == EditMode.TRI ) help.text += "<color=#F00>";
		help.text += "  <T> - Triangulate\n";
		if ( mode == EditMode.TRI ) help.text += "</color>";
		
		help.text += "<LMB> - Make Vertex\n";
	}
}