// EQUATIONS:
//-------------------------------------
// area = x * y;
// uvArea = mx * my;
// mx = m * x;
// my = m * y;
//
// where
// x,y - size of origial uv aabb
// my,my - size of final uv aabb
// uvArea - area of final uv aabb, that is constant across all mesh objects
// m - unknown
//
// SOLVING mx * my = marea FOR m:
//-------------------------------------
// mx * my = uvArea;
// m^2 * (x * y) = uvArea;
// m^2 = uvArea / (x * y);
// m = sqrt( uvArea / (x * y) );


m^2 * area = const_area;

full_mesh_area = 0;
foreach ( tri in triangles )
{
	full_mesh_area += area( tri );
}

area( tri )
{
	return ( (v2-v1) dot (v3-v1) ) / 2;
}

area( tri, m )
{
	return ( m*(v2-v1) dot m*(v3-v1) ) / 2;
}